# Tema Transfer Front-End  

### **Bună siua tributelor!**  


**Bine ați venit la minunatele jocuri ale foamei, suntem gazdele voastre Cristi El Guaie și Astrid Crocox.**  
În cele ce urmează noi vă vom prezenta prin ce încercări **_periculoase_** o să treceți, plus un mic tutorial pe parcurs.  

#### **Materiale pentru divizia Front-end**  

Încercaţi să parcurgeţi cât de mult puteţi, iar dacă nu vă permite timpul, măcar noţiunile de bază.  
Nu vă speriaţi dacă întâlniţi noţiuni complicate sau dacă nu înţelegeţi ceva, sunt multe lucruri pe care nici noi poate nu le ştim.   
Scopul e să vă acomodaţi şi să începeţi să aveţi o imagine de ansamblu a tehnologiilor pe care le folosim.  
  
#### **Manuscrise**  
  
**Acestea sunt câteva manuscrise cu materialele pe care vi le propunem:**  
>$ [**HTML & CSS**](https://www.khanacademy.org/computing/computer-programming/html-css) pentru a trece prin toate noțiunile de bază, până la secțiunea  Web development tools.  
>$ [**JavaScript**](http://jsforcats.com/) pentru noțiuni de bază și introductive în minunatele lumi ale JS-ului.  
  
#### **Arme_și_unelte**    

**În ceea ce privește armele voastre, adică ustensilele de lucru, voi aveți nevoie de:**  
>$ [**Visual Studio Code**](https://code.visualstudio.com/) un minunat editor text ce vă va ajuta să vă luptați eficient cu taskurile  
>$ [**Git**](http://rogerdudler.github.io/git-guide/) această unealtă este primordială ca piatra filosofală sau ceva de genu nu sunt Arhimede, și vine cu un tutorial atașat.    

#### **Confruntarea**   

Ei bine, a sosit momentul în care voi, tributele din districtele voastre natale, vă luptați pentru supraviețuire cu taskurile Front.  
Scopul vostru acum, este să vă camuflați cât de bine puteți în natură, astfel voi trebuie sa realizați copia funcțională a acestui mockup.  
El, mockup-ul se află în cufarul **_website_** și pentru a îl realiza, voi veți folosi vopselurile din folderul **_assets_**.  
**în assets găsiți:**    
>$ pozele de care aveți nevoie,  
>$ link-uri către site-uri cu fonturi,  
>$ link-uri către site-uri cu iconițe + how to use them.    

La formularul din pagină, nu uitați să adăugați cateva **toast notifications** pentru validarea formularului.    
[**toastr**](https://github.com/CodeSeven/toastr) *aici ne puteți întreba dacă nu ințelegeți*  
**++taburile din meniu trebie să ducă la secțiunea respectivă!**  

#### **Provocarea_finală**  

Ați ajuns și aici. După ce ați luptat aprig urmează **The Final Showdown** voi trebuie să faceți site-ul vostru responsive pentru mobile.  
[**Responsiveness**](https://www.w3schools.com/css/css_rwd_mediaqueries.asp) este unul dintre cuvintele cheie la noi în departament și vrem să vedem dacă reușiți să ne surprindeți.

*NU uitați, Google este mereu de partea voastră*   
  *Probleme la implementare?*    
  **Contacts -> Departments -> IT -> Munteanu Cristian/Staicu Astrid/Eftene Robert**  
  
#### **Fe_Li_Na**  
Așadar, pentru a trimite tema, va trebui să vă faceți un cont de GitLab, pe care să urcați proiectul vostru. Mai departe ne veți da prin email un link către acesta alături de motivația scrisă și un filmuleț în care vreți să ne arătați "De ce vreți să fiți în IT?".   
Apoi, veți trimite tot la adresa munteanu.cristian1009@gmail.com , iar în CC veți pune adresa staicuastrid2000@gmail.com .  

  **Mult spor! And may the mocking birds be with you!**  






